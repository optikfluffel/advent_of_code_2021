defmodule AdventOfCode2021 do
  @moduledoc """
  Documentation for `AdventOfCode2021`.
  """

  def solve_day01 do
    input = File.read!("inputs/day01.txt")

    part1 = AdventOfCode2021.Day01.count_increases(input)
    part2 = AdventOfCode2021.Day01.count_increases_sliding_window(input)

    {part1, part2}
  end

  def solve_day02 do
    input = File.read!("inputs/day02.txt")

    part1 = AdventOfCode2021.Day02.calculate(input)
    part2 = AdventOfCode2021.Day02.calculate_with_aim(input)

    {part1, part2}
  end

  def solve_day03 do
    input = File.read!("inputs/day03.txt")

    part1 = AdventOfCode2021.Day03.power_consumption(input)
    part2 = AdventOfCode2021.Day03.life_support_rating(input)

    {part1, part2}
  end

  def solve_day04 do
    input = File.read!("inputs/day04.txt")

    part1 = AdventOfCode2021.Day04.play_bingo(input)
    part2 = AdventOfCode2021.Day04.last_winning_score(input)

    {part1, part2}
  end

  def solve_day05 do
    input = File.read!("inputs/day05.txt")

    part1 = AdventOfCode2021.Day05.count_dangerous_points(input)
    part2 = AdventOfCode2021.Day05.count_dangerous_points_with_diagonal(input)

    {part1, part2}
  end

  def solve_day06 do
    input = File.read!("inputs/day06.txt")

    part1 = AdventOfCode2021.Day06.simulate_lanternfish(input)
    part2 = AdventOfCode2021.Day06.simulate_lanternfish_longer(input)

    {part1, part2}
  end

  def solve_day07 do
    input = File.read!("inputs/day07.txt")

    part1 = AdventOfCode2021.Day07.least_fuel(input)
    part2 = AdventOfCode2021.Day07.least_fuel_rated(input)

    {part1, part2}
  end

  def solve_day08 do
    input = File.read!("inputs/day08.txt")

    part1 = AdventOfCode2021.Day08.count_relevant_output_digits(input)
    part2 = AdventOfCode2021.Day08.sum_of_decoded_outputs(input)

    {part1, part2}
  end

  def solve_day09 do
    input = File.read!("inputs/day09.txt")

    part1 = AdventOfCode2021.Day09.sum_of_risk_levels(input)
    part2 = AdventOfCode2021.Day09.multiply_three_largest_basins(input)

    {part1, part2}
  end

  def solve_day10 do
    input = File.read!("inputs/day10.txt")

    part1 = AdventOfCode2021.Day10.total_syntax_error_score(input)
    part2 = AdventOfCode2021.Day10.middle_score(input)

    {part1, part2}
  end

  def solve_day11 do
    input = File.read!("inputs/day11.txt")

    part1 = AdventOfCode2021.Day11.total_number_of_flashes(input)
    part2 = AdventOfCode2021.Day11.first_simultaneously_flash(input)

    {part1, part2}
  end

  def solve_day14 do
    input = File.read!("inputs/day14.txt")

    part1 = AdventOfCode2021.Day14.most_minus_least_common_quantity(input)
    part2 = AdventOfCode2021.Day14.most_minus_least_common_quantity(input, 40)

    {part1, part2}
  end
end
