defmodule AdventOfCode2021.Day08 do
  def count_relevant_output_digits(input) do
    input |> parse() |> Enum.reduce(0, &count_relevant/2)
  end

  def sum_of_decoded_outputs(input) do
    input |> parse() |> Enum.map(&unscramble_single_line/1) |> Enum.sum()
  end

  defp unscramble_single_line({signal, output}) do
    %{
      2 => [one],
      3 => [seven],
      4 => [four],
      5 => two_three_five,
      6 => zero_six_nine,
      7 => [eight]
    } = signal |> Enum.group_by(&length/1)

    [six] = zero_six_nine |> Enum.filter(&(length(&1 -- seven) == 4))

    zero_nine = zero_six_nine -- [six]
    [nine] = zero_nine |> Enum.filter(&(length(&1 -- four) == 2))
    [zero] = zero_nine |> Enum.filter(&(length(&1 -- four) == 3))

    [three] = two_three_five |> Enum.filter(&(length(&1 -- seven) == 2))

    two_five = two_three_five -- [three]
    [two] = two_five |> Enum.filter(&(length(&1 -- four) == 3))
    [five] = two_five |> Enum.filter(&(length(&1 -- four) == 2))

    mapping =
      [zero, one, two, three, four, five, six, seven, eight, nine]
      |> Enum.with_index()
      |> Enum.into(%{})

    output |> Enum.map(&Map.get(mapping, &1)) |> Integer.undigits()
  end

  defp parse(input) do
    input |> String.split("\n", trim: true) |> Enum.map(&parse_line/1)
  end

  defp parse_line(input) do
    [signal, output] =
      input
      |> String.split("|", trim: true, parts: 2)
      |> Enum.map(&String.split(&1, " ", trim: true))
      |> Enum.map(fn x -> x |> Enum.map(&String.graphemes/1) |> Enum.map(&Enum.sort/1) end)

    {signal, output}
  end

  defp count_relevant({_signal, output}, count) do
    count + Enum.count(output, &has_relevant_length/1)
  end

  defp has_relevant_length(list), do: length(list) in [2, 3, 4, 7]
end
