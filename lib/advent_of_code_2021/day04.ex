defmodule AdventOfCode2021.Day04 do
  def play_bingo(input) do
    {sequence, unmarked_boards} = parse_input(input)

    {won, last_number} = Enum.reduce_while(sequence, {unmarked_boards, 0}, &play_round/2)

    calculate_score(won, last_number)
  end

  def last_winning_score(input) do
    {sequence, unmarked_boards} = parse_input(input)

    {last_won, last_number} = Enum.reduce_while(sequence, {unmarked_boards, 0}, &remove_winning/2)

    calculate_score(last_won, last_number)
  end

  defp parse_input(input) do
    [sequence_str | boards_str] = String.split(input, "\n\n", trim: true)

    sequence = String.split(sequence_str, ",") |> Enum.map(&String.to_integer/1)
    boards = boards_str |> Enum.map(&parse_single_board/1)

    {sequence, boards}
  end

  defp parse_single_board(board_str) do
    board_str
    |> String.split("\n", trim: true)
    |> Enum.map(&parse_single_row/1)
  end

  defp parse_single_row(row_str) do
    row_str
    |> String.split(" ", trim: true)
    |> Enum.map(fn number_str -> {String.to_integer(number_str), :unmarked} end)
  end

  defp play_round(number, {boards, _last_number}) do
    marked_boards = mark(boards, number)

    case winning_board(marked_boards) do
      :none -> {:cont, {marked_boards, number}}
      winning_board -> {:halt, {winning_board, number}}
    end
  end

  defp remove_winning(number, {[_last_board] = boards, _last_number}) do
    [marked_board] = marked_boards = mark(boards, number)

    case has_full_row_or_column?(marked_board) do
      true -> {:halt, {marked_board, number}}
      false -> {:cont, {marked_boards, number}}
    end
  end

  defp remove_winning(number, {boards, _last_number}) do
    remaining =
      boards
      |> mark(number)
      |> Enum.reject(&has_full_row_or_column?/1)

    {:cont, {remaining, number}}
  end

  defp mark(boards, number) do
    Enum.map(boards, fn board ->
      Enum.map(board, &mark_row(&1, number))
    end)
  end

  defp mark_row(row, number) do
    Enum.map(row, fn
      {^number, :unmarked} -> {number, :marked}
      unchanged -> unchanged
    end)
  end

  defp winning_board(boards) do
    case Enum.find(boards, &has_full_row_or_column?/1) do
      nil -> :none
      board -> board
    end
  end

  defp has_full_row_or_column?(board) do
    has_full_row?(board) or has_full_column?(board)
  end

  defp has_full_row?(board) do
    Enum.any?(board, fn row ->
      Enum.all?(row, &is_marked?/1)
    end)
  end

  defp has_full_column?(board) do
    Enum.any?(0..4, fn index ->
      Enum.all?(board, fn row ->
        row |> Enum.at(index) |> is_marked?()
      end)
    end)
  end

  defp calculate_score(board, number) do
    board
    |> List.flatten()
    |> Enum.filter(&is_unmarked?/1)
    |> Enum.map(&elem(&1, 0))
    |> Enum.sum()
    |> Kernel.*(number)
  end

  defp is_marked?({_number, :marked}), do: true
  defp is_marked?(_), do: false

  defp is_unmarked?({_number, :unmarked}), do: true
  defp is_unmarked?(_), do: false
end
