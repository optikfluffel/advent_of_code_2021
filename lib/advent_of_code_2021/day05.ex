defmodule AdventOfCode2021.Day05 do
  def count_dangerous_points(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.map(&parse_line/1)
    |> Enum.reduce(%{}, &mark_vents/2)
    |> Enum.count(fn {_key, value} -> value >= 2 end)
  end

  def count_dangerous_points_with_diagonal(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.map(&parse_line/1)
    |> Enum.reduce(%{}, &mark_vents_with_diagonal/2)
    |> Enum.count(fn {_key, value} -> value >= 2 end)
  end

  defp parse_line(str) do
    [from, to] =
      str
      |> String.split("->", trim: true, parts: 2)
      |> Enum.map(&String.split(&1, ",", trim: true))
      |> Enum.map(&Enum.map(&1, fn x -> x |> String.trim() |> String.to_integer() end))
      |> Enum.map(fn [x, y] -> {x, y} end)

    {from, to}
  end

  defp mark_vents({{x1, y}, {x2, y}}, map) do
    x1..x2
    |> Enum.map(&{&1, y})
    |> Enum.reduce(map, &update_map/2)
  end

  defp mark_vents({{x, y1}, {x, y2}}, map) do
    y1..y2
    |> Enum.map(&{x, &1})
    |> Enum.reduce(map, &update_map/2)
  end

  defp mark_vents(_input, map) do
    map
  end

  defp mark_vents_with_diagonal({{x1, x1}, {x2, x2}}, map) do
    x1..x2
    |> Enum.map(&{&1, &1})
    |> Enum.reduce(map, &update_map/2)
  end

  defp mark_vents_with_diagonal({{_x1, y}, {_x2, y}} = coords, map), do: mark_vents(coords, map)
  defp mark_vents_with_diagonal({{x, _y1}, {x, _y2}} = coords, map), do: mark_vents(coords, map)

  defp mark_vents_with_diagonal({{x1, y1}, {x2, y2}}, map) do
    x1..x2
    |> Enum.zip(y1..y2)
    |> List.flatten()
    |> Enum.reduce(map, &update_map/2)
  end

  defp update_map({x, y}, map) do
    Map.update(map, {x, y}, 1, fn value -> value + 1 end)
  end
end
