defmodule AdventOfCode2021.Day09 do
  def sum_of_risk_levels(input) do
    input
    |> parse()
    |> only_low_points()
    |> Enum.map(&elem(&1, 1))
    |> Enum.map(&(&1 + 1))
    |> Enum.sum()
  end

  def multiply_three_largest_basins(input) do
    map = parse(input)

    map
    |> only_low_points()
    |> Enum.map(&collect_basin([&1], map))
    |> Enum.map(&Enum.count/1)
    |> Enum.sort(:desc)
    |> Enum.take(3)
    |> Enum.reduce(1, &Kernel.*/2)
  end

  defp parse(input) do
    input |> String.split("\n", trim: true) |> Enum.map(&parse_line/1) |> into_map()
  end

  defp parse_line(line) do
    line |> String.graphemes() |> Enum.map(&String.to_integer/1)
  end

  defp into_map(rows) do
    for {row, row_index} <- Enum.with_index(rows) do
      for {value, column_index} <- Enum.with_index(row) do
        {{row_index, column_index}, value}
      end
    end
    |> List.flatten()
    |> Enum.into(%{})
  end

  defp only_low_points(%{} = map) do
    Enum.filter(map, &smaller_than_all_neighboors(&1, map))
  end

  defp smaller_than_all_neighboors({coords, value}, map) do
    coords
    |> neighboor_coords()
    |> Enum.all?(fn neighboor_coords ->
      case Map.get(map, neighboor_coords) do
        nil -> true
        v -> v > value
      end
    end)
  end

  defp neighboor_coords({x, y}), do: [{x - 1, y}, {x + 1, y}, {x, y - 1}, {x, y + 1}]

  defp collect_basin(points, map, collected \\ [])
  defp collect_basin([], _map, collected), do: collected

  defp collect_basin(points, map, collected) do
    valid_points = points |> Enum.filter(fn {_coords, value} -> value < 9 end)

    new_neighboors =
      valid_points
      |> Enum.map(&elem(&1, 0))
      |> Enum.flat_map(&neighboor_coords/1)
      |> Enum.reduce([], fn coords, acc ->
        case Map.get(map, coords) do
          nil ->
            acc

          value ->
            point = {coords, value}

            if point in collected or point in acc do
              acc
            else
              [point | acc]
            end
        end
      end)

    collect_basin(new_neighboors, map, collected ++ valid_points)
  end
end
