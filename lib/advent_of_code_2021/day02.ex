defmodule AdventOfCode2021.Day02 do
  def calculate(input) do
    {position, depth} =
      input
      |> String.split("\n", trim: true)
      |> Enum.map(&parse_line/1)
      |> Enum.reduce({0, 0}, &move/2)

    position * depth
  end

  def calculate_with_aim(input) do
    {position, depth, _aim} =
      input
      |> String.split("\n", trim: true)
      |> Enum.map(&parse_line/1)
      |> Enum.reduce({0, 0, 0}, &move_with_aim/2)

    position * depth
  end

  defp parse_line("forward " <> n), do: {:forward, String.to_integer(n)}
  defp parse_line("up " <> n), do: {:up, String.to_integer(n)}
  defp parse_line("down " <> n), do: {:down, String.to_integer(n)}

  defp move({:forward, n}, {pos, depth}), do: {pos + n, depth}
  defp move({:up, n}, {pos, depth}), do: {pos, depth - n}
  defp move({:down, n}, {pos, depth}), do: {pos, depth + n}

  defp move_with_aim({:forward, n}, {pos, depth, aim}), do: {pos + n, depth + aim * n, aim}
  defp move_with_aim({:up, n}, {pos, depth, aim}), do: {pos, depth, aim - n}
  defp move_with_aim({:down, n}, {pos, depth, aim}), do: {pos, depth, aim + n}
end
