defmodule AdventOfCode2021.Day03 do
  def power_consumption(input) do
    bit_list = input |> String.split("\n", trim: true) |> Enum.map(&parse_line/1)
    bit_size = bit_list |> List.first() |> bit_size()

    {gamma_rate_bits, epsilon_rate_bits} =
      Enum.reduce(0..(bit_size - 1), {<<>>, <<>>}, fn index, {gamma_rate, epsilon_rate} ->
        case count_zeros_and_ones(bit_list, index) do
          {number_of_zeros, number_of_ones} when number_of_zeros < number_of_ones ->
            {<<gamma_rate::bitstring, 1::1>>, <<epsilon_rate::bitstring, 0::1>>}

          {_number_of_zeros, _number_of_ones} ->
            {<<gamma_rate::bitstring, 0::1>>, <<epsilon_rate::bitstring, 1::1>>}
        end
      end)

    bitstring_to_integer(gamma_rate_bits) * bitstring_to_integer(epsilon_rate_bits)
  end

  def life_support_rating(input) do
    bit_list = input |> String.split("\n", trim: true) |> Enum.map(&parse_line/1)

    oxygen_generator_bits = find_oxygen_generator_rating(bit_list, 0)
    co2_scrubber_bits = find_co2_scrubber_rating(bit_list, 0)

    bitstring_to_integer(oxygen_generator_bits) * bitstring_to_integer(co2_scrubber_bits)
  end

  defp find_oxygen_generator_rating([final], _index), do: final

  defp find_oxygen_generator_rating(list, index) do
    most_common =
      case count_zeros_and_ones(list, index) do
        {number_of_zeros, number_of_ones} when number_of_zeros <= number_of_ones -> 1
        {_number_of_zeros, _number_of_ones} -> 0
      end

    list
    |> Enum.filter(fn
      <<_skip::size(index), ^most_common::1, _rest::bitstring>> -> true
      _ -> false
    end)
    |> find_oxygen_generator_rating(index + 1)
  end

  defp find_co2_scrubber_rating([final], _index), do: final

  defp find_co2_scrubber_rating(list, index) do
    least_common =
      case count_zeros_and_ones(list, index) do
        {number_of_zeros, number_of_ones} when number_of_zeros <= number_of_ones -> 0
        {_number_of_zeros, _number_of_ones} -> 1
      end

    list
    |> Enum.filter(fn
      <<_skip::size(index), ^least_common::1, _rest::bitstring>> -> true
      _ -> false
    end)
    |> find_co2_scrubber_rating(index + 1)
  end

  defp parse_line(string, acc \\ <<>>)
  defp parse_line("1" <> rest, acc), do: parse_line(rest, <<acc::bitstring, 1::1>>)
  defp parse_line("0" <> rest, acc), do: parse_line(rest, <<acc::bitstring, 0::1>>)
  defp parse_line("", acc), do: acc

  defp count_zeros_and_ones(list, index) do
    %{0 => number_of_zeros, 1 => number_of_ones} =
      list
      |> Enum.frequencies_by(fn <<_skip::size(index), x::1, _rest::bitstring>> -> x end)
      |> Map.put_new(0, 0)
      |> Map.put_new(1, 0)

    {number_of_zeros, number_of_ones}
  end

  defp bitstring_to_integer(bs) when is_binary(bs), do: :binary.decode_unsigned(bs)

  defp bitstring_to_integer(bs) when is_bitstring(bs) do
    pad_length = 8 - rem(bit_size(bs), 8)
    binary = <<0::size(pad_length), bs::bitstring>>
    bitstring_to_integer(binary)
  end
end
