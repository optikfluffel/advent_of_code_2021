defmodule AdventOfCode2021.Day11 do
  def total_number_of_flashes(input) do
    input |> parse() |> count_flashes(100, 0)
  end

  def first_simultaneously_flash(input) do
    input |> parse() |> find_first_simultaneously_flash(0)
  end

  defp parse(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.with_index()
    |> Enum.flat_map(&parse_line/1)
    |> Enum.into(%{})
  end

  defp parse_line({str, row}) do
    str
    |> String.graphemes()
    |> Enum.map(&String.to_integer/1)
    |> Enum.with_index()
    |> Enum.map(fn {energy_level, column} -> {{column, row}, energy_level} end)
  end

  defp count_flashes(_map, 0, flashes), do: flashes

  defp count_flashes(map, rounds, flashes) do
    next_map = step(map)

    new_flashes =
      next_map
      |> Enum.map(&elem(&1, 1))
      |> Enum.count(fn energy_level -> energy_level == :flashing end)

    next_map
    |> Enum.map(&reset_flashing/1)
    |> Enum.into(%{})
    |> count_flashes(rounds - 1, flashes + new_flashes)
  end

  defp find_first_simultaneously_flash(map, rounds) do
    case map |> Enum.map(&elem(&1, 1)) |> Enum.all?(fn el -> el == :flashing end) do
      true ->
        rounds

      false ->
        map
        |> Enum.map(&reset_flashing/1)
        |> Enum.into(%{})
        |> step()
        |> find_first_simultaneously_flash(rounds + 1)
    end
  end

  defp step(map) do
    map
    |> plus_one_all()
    |> flash()
  end

  defp plus_one_all(map), do: map |> Map.keys() |> Enum.reduce(map, &plus_one_at/2)

  defp plus_one_at(coords, map) do
    Map.update!(map, coords, fn
      :flashing -> :flashing
      value -> value + 1
    end)
  end

  defp flash(map) do
    case Enum.filter(map, &bigger_nine?/1) do
      [] ->
        map

      new_flashing ->
        flashing_map =
          new_flashing
          |> Enum.map(&elem(&1, 0))
          |> Enum.reduce(map, &mark_flashing/2)

        new_flashing
        |> Enum.flat_map(&get_neighboor_coords(&1, flashing_map))
        |> Enum.reduce(flashing_map, &plus_one_at/2)
        |> flash()
    end
  end

  defp bigger_nine?({_coords, energy_level}) when is_integer(energy_level) and energy_level > 9,
    do: true

  defp bigger_nine?(_octopus), do: false

  defp mark_flashing(coords, acc), do: Map.put(acc, coords, :flashing)

  defp reset_flashing({coords, :flashing}), do: {coords, 0}
  defp reset_flashing(octopus), do: octopus

  defp get_neighboor_coords({{x, y}, _energy_level}, map) do
    [
      {x + 1, y},
      {x - 1, y},
      {x, y + 1},
      {x, y - 1},
      {x + 1, y + 1},
      {x + 1, y - 1},
      {x - 1, y + 1},
      {x - 1, y - 1}
    ]
    |> Enum.filter(fn coords ->
      case Map.get(map, coords) do
        nil -> false
        :flashing -> false
        _energy_level -> true
      end
    end)
  end
end
