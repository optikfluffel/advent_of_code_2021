defmodule AdventOfCode2021.Day14 do
  def most_minus_least_common_quantity(input, steps \\ 10) do
    [template_str, rules_str] = input |> String.split("\n\n", trim: true, parts: 2)

    template = String.graphemes(template_str)
    last = List.last(template)

    rules =
      rules_str
      |> String.split("\n", trim: true)
      |> Enum.map(fn rule_str ->
        [from, to] = rule_str |> String.split(" -> ", parts: 2)

        {String.graphemes(from), to}
      end)
      |> Enum.into(%{})

    initial_acc = template |> Enum.chunk_every(2, 1, :discard) |> Enum.frequencies()

    {{_min_letter, min}, {_max_letter, max}} =
      1..steps
      |> Enum.reduce({initial_acc, rules}, &step/2)
      |> elem(0)
      |> Enum.reduce(%{last => 1}, &sum_left_counts/2)
      |> Enum.min_max_by(&elem(&1, 1))

    max - min
  end

  defp step(_step, {acc, rules}) do
    Enum.reduce(acc, {%{}, rules}, &insert/2)
  end

  defp insert({[left, right] = pair, count}, {acc, rules}) do
    left_pair = [left, Map.fetch!(rules, pair)]
    right_pair = [Map.fetch!(rules, pair), right]

    next_acc =
      acc
      |> Map.update(left_pair, count, &(&1 + count))
      |> Map.update(right_pair, count, &(&1 + count))

    {next_acc, rules}
  end

  defp sum_left_counts({[left, _right], count}, acc) do
    Map.update(acc, left, count, &(&1 + count))
  end
end
