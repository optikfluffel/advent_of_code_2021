defmodule AdventOfCode2021.Day01 do
  def count_increases_sliding_window(input) do
    [first | rest] =
      input
      |> String.split("\n", trim: true)
      |> Enum.map(&String.to_integer/1)
      |> sum_sliding_windows([])

    count_increases(rest, {0, first})
  end

  def count_increases(input) do
    [first | rest] = input |> String.split("\n", trim: true) |> Enum.map(&String.to_integer/1)

    count_increases(rest, {0, first})
  end

  defp count_increases([], {count, _previous}), do: count

  defp count_increases([first | rest], {count, previous}) when first > previous do
    count_increases(rest, {count + 1, first})
  end

  defp count_increases([first | rest], {count, _previous}) do
    count_increases(rest, {count, first})
  end

  defp sum_sliding_windows([_first | rest] = list, acc) when length(list) >= 3 do
    sum = list |> Enum.take(3) |> Enum.sum()

    sum_sliding_windows(rest, [sum | acc])
  end

  defp sum_sliding_windows(_list, acc), do: Enum.reverse(acc)
end
