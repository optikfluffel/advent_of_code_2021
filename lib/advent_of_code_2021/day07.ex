defmodule AdventOfCode2021.Day07 do
  def least_fuel(input) do
    positions = input |> parse()
    {min, max} = Enum.min_max(positions)

    min..max |> Enum.map(&fuel_consumption(positions, &1)) |> Enum.min()
  end

  def least_fuel_rated(input) do
    positions = input |> parse()
    {min, max} = Enum.min_max(positions)

    min..max |> Enum.map(&fuel_consumption_rated(positions, &1)) |> Enum.min()
  end

  defp parse(input) do
    input |> String.trim() |> String.split(",") |> Enum.map(&String.to_integer/1)
  end

  defp fuel_consumption(positions, target) do
    positions |> Enum.map(&fuel_requirement(&1, target)) |> Enum.sum()
  end

  defp fuel_consumption_rated(positions, target) do
    positions |> Enum.map(&fuel_requirement_rated(&1, target)) |> Enum.sum()
  end

  defp fuel_requirement(position, target) when position < target, do: target - position
  defp fuel_requirement(position, target) when position > target, do: position - target
  defp fuel_requirement(position, position), do: 0

  defp fuel_requirement_rated(pos, target) when pos < target, do: sum_of_n_numbers(target - pos)
  defp fuel_requirement_rated(pos, target) when pos > target, do: sum_of_n_numbers(pos - target)
  defp fuel_requirement_rated(pos, pos), do: 0

  # thanks Gauss <3
  defp sum_of_n_numbers(n), do: round(n * (n + 1) / 2)
end
