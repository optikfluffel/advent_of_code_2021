defmodule AdventOfCode2021.Day06 do
  @empty_state 0..8 |> Enum.map(&{&1, 0}) |> Enum.into(%{})

  def simulate_lanternfish(input) do
    input |> parse() |> initial_state() |> simulate(80)
  end

  def simulate_lanternfish_longer(input) do
    input |> parse() |> initial_state() |> simulate(256)
  end

  defp initial_state(initial_fish), do: Map.merge(@empty_state, initial_fish)

  defp parse(input) do
    input
    |> String.trim()
    |> String.split(",")
    |> Enum.map(&String.to_integer/1)
    |> Enum.frequencies()
  end

  defp simulate(state, days) do
    1..days
    |> Enum.reduce(state, &simulate_growth/2)
    |> Enum.map(&elem(&1, 1))
    |> Enum.sum()
  end

  defp simulate_growth(_day, state) do
    state |> Enum.sort_by(&elem(&1, 0), &>=/2) |> Enum.reduce(state, &simulate_day/2)
  end

  defp simulate_day({0, n}, fish) do
    fish |> Map.update(6, 0, fn x -> x + n end) |> Map.put(8, n)
  end

  defp simulate_day({key, n}, fish) do
    fish |> Map.put(key - 1, n)
  end
end
