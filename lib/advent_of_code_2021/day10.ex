defmodule AdventOfCode2021.Day10 do
  @opening ["(", "[", "{", "<"]
  @closing [")", "]", "}", ">"]

  def total_syntax_error_score(input) do
    input
    |> parse()
    |> Enum.reduce([], &collect_incorrect_closing_character/2)
    |> Enum.reduce(0, &syntax_error_score/2)
  end

  def middle_score(input) do
    sorted_scores =
      input
      |> parse()
      |> Enum.filter(&valid_line?/1)
      |> Enum.map(&collect_missing_closing/1)
      |> Enum.map(&autocomplete_score/1)
      |> Enum.sort()

    middle = sorted_scores |> length() |> div(2)
    Enum.at(sorted_scores, middle)
  end

  defp parse(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.map(&String.graphemes/1)
  end

  defp collect_incorrect_closing_character(line, acc) do
    case validate_line(line, []) do
      :ok -> acc
      {:error, {:invalid_colsing_character, char}} -> [char | acc]
    end
  end

  defp validate_line([], _opened), do: :ok

  defp validate_line([current | rest], opened) when current in @opening do
    validate_line(rest, [current | opened])
  end

  defp validate_line([current | rest], [last_opened | rest_opened]) when current in @closing do
    case valid_pair?(last_opened, current) do
      true -> validate_line(rest, rest_opened)
      false -> {:error, {:invalid_colsing_character, current}}
    end
  end

  defp valid_pair?("(", ")"), do: true
  defp valid_pair?("[", "]"), do: true
  defp valid_pair?("{", "}"), do: true
  defp valid_pair?("<", ">"), do: true
  defp valid_pair?(_, _), do: false

  defp syntax_error_score(")", acc), do: acc + 3
  defp syntax_error_score("]", acc), do: acc + 57
  defp syntax_error_score("}", acc), do: acc + 1197
  defp syntax_error_score(">", acc), do: acc + 25137

  defp valid_line?(line) do
    case validate_line(line, []) do
      :ok -> true
      {:error, _error} -> false
    end
  end

  defp collect_missing_closing(line, collected \\ [])
  defp collect_missing_closing([], collected), do: collected

  defp collect_missing_closing([current | rest], opened) when current in @opening do
    collect_missing_closing(rest, [current | opened])
  end

  defp collect_missing_closing([current | rest], [_ | rest_opened]) when current in @closing do
    collect_missing_closing(rest, rest_opened)
  end

  defp autocomplete_score(unclosed) do
    Enum.reduce(unclosed, 0, fn x, acc ->
      score =
        case x do
          "(" -> 1
          "[" -> 2
          "{" -> 3
          "<" -> 4
        end

      acc * 5 + score
    end)
  end
end
