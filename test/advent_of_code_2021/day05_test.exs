defmodule AdventOfCode2021.Day05Test do
  use ExUnit.Case, async: true

  alias AdventOfCode2021.Day05

  @input """
  0,9 -> 5,9
  8,0 -> 0,8
  9,4 -> 3,4
  2,2 -> 2,1
  7,0 -> 7,4
  6,4 -> 2,0
  0,9 -> 2,9
  3,4 -> 1,4
  0,0 -> 8,8
  5,5 -> 8,2
  """

  test "count_dangerous_points/1" do
    assert 5 == Day05.count_dangerous_points(@input)
  end

  test "count_dangerous_points_with_diagonal/1" do
    assert 12 == Day05.count_dangerous_points_with_diagonal(@input)
  end
end
