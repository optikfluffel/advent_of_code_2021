defmodule AdventOfCode2021.Day14Test do
  use ExUnit.Case, async: true

  alias AdventOfCode2021.Day14

  @input """
  NNCB

  CH -> B
  HH -> N
  CB -> H
  NH -> C
  HB -> C
  HC -> B
  HN -> C
  NN -> C
  BH -> H
  NC -> B
  NB -> B
  BN -> B
  BB -> N
  BC -> B
  CC -> N
  CN -> C
  """

  test "most_minus_least_common_quantity/1" do
    assert 1588 == Day14.most_minus_least_common_quantity(@input)
  end

  test "most_minus_least_common_quantity/2" do
    assert 2_188_189_693_529 == Day14.most_minus_least_common_quantity(@input, 40)
  end
end
