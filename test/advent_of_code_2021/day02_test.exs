defmodule AdventOfCode2021.Day02Test do
  use ExUnit.Case, async: true

  alias AdventOfCode2021.Day02

  @input """
  forward 5
  down 5
  forward 8
  up 3
  down 8
  forward 2
  """

  test "calculate/1" do
    assert 150 == Day02.calculate(@input)
  end

  test "calculate_with_aim/1" do
    assert 900 == Day02.calculate_with_aim(@input)
  end
end
