defmodule AdventOfCode2021.Day11Test do
  use ExUnit.Case, async: true

  alias AdventOfCode2021.Day11

  @input """
  5483143223
  2745854711
  5264556173
  6141336146
  6357385478
  4167524645
  2176841721
  6882881134
  4846848554
  5283751526
  """

  test "total_number_of_flashes/1" do
    assert 1656 == Day11.total_number_of_flashes(@input)
  end

  test "first_simultaneously_flash/1" do
    assert 195 == Day11.first_simultaneously_flash(@input)
  end
end
