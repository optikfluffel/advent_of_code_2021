defmodule AdventOfCode2021.Day03Test do
  use ExUnit.Case, async: true

  alias AdventOfCode2021.Day03

  @input """
  00100
  11110
  10110
  10111
  10101
  01111
  00111
  11100
  10000
  11001
  00010
  01010
  """

  test "power_consumption/1" do
    assert 198 == Day03.power_consumption(@input)
  end

  test "life_support_rating/1" do
    assert 230 == Day03.life_support_rating(@input)
  end
end
