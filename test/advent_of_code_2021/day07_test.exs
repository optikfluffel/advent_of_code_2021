defmodule AdventOfCode2021.Day07Test do
  use ExUnit.Case, async: true

  alias AdventOfCode2021.Day07

  @input """
  16,1,2,0,4,2,7,1,2,14
  """

  test "least_fuel/1" do
    assert 37 == Day07.least_fuel(@input)
  end

  test "least_fuel_rated/1" do
    assert 168 == Day07.least_fuel_rated(@input)
  end
end
