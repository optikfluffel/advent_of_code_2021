defmodule AdventOfCode2021.Day10Test do
  use ExUnit.Case, async: true

  alias AdventOfCode2021.Day10

  @input """
  [({(<(())[]>[[{[]{<()<>>
  [(()[<>])]({[<{<<[]>>(
  {([(<{}[<>[]}>{[]{[(<()>
  (((({<>}<{<{<>}{[]{[]{}
  [[<[([]))<([[{}[[()]]]
  [{[{({}]{}}([{[{{{}}([]
  {<[[]]>}<{[{[{[]{()[[[]
  [<(<(<(<{}))><([]([]()
  <{([([[(<>()){}]>(<<{{
  <{([{{}}[<[[[<>{}]]]>[]]
  """

  test "total_syntax_error_score/1" do
    assert 26397 == Day10.total_syntax_error_score(@input)
  end

  test "middle score/1" do
    assert 288_957 == Day10.middle_score(@input)
  end
end
