defmodule AdventOfCode2021.Day06Test do
  use ExUnit.Case, async: true

  alias AdventOfCode2021.Day06

  @input """
  3,4,3,1,2
  """

  test "simulate_lanternfish/1" do
    assert 5934 == Day06.simulate_lanternfish(@input)
  end

  test "simulate_lanternfish_longer/1" do
    assert 26_984_457_539 == Day06.simulate_lanternfish_longer(@input)
  end
end
