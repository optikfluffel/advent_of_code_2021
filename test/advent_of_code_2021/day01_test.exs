defmodule AdventOfCode2021.Day01Test do
  use ExUnit.Case, async: true

  alias AdventOfCode2021.Day01

  @input """
  199
  200
  208
  210
  200
  207
  240
  269
  260
  263
  """

  test "count_increases/1" do
    assert 7 == Day01.count_increases(@input)
  end

  test "count_increases_sliding_window/1" do
    assert 5 == Day01.count_increases_sliding_window(@input)
  end
end
