defmodule AdventOfCode2021.Day09Test do
  use ExUnit.Case, async: true

  alias AdventOfCode2021.Day09

  @input """
  2199943210
  3987894921
  9856789892
  8767896789
  9899965678
  """

  test "sum_of_risk_levels/1" do
    assert 15 == Day09.sum_of_risk_levels(@input)
  end

  test "multiply_three_largest_basins/1" do
    assert 1134 == Day09.multiply_three_largest_basins(@input)
  end
end
