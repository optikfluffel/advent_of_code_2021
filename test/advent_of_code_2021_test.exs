defmodule AdventOfCode2021Test do
  use ExUnit.Case, async: true

  test "solve_day01/0" do
    assert {1688, 1728} == AdventOfCode2021.solve_day01()
  end

  test "solve_day02/0" do
    assert {1_868_935, 1_965_970_888} == AdventOfCode2021.solve_day02()
  end

  test "solve_day03/0" do
    assert {3_959_450, 7_440_311} == AdventOfCode2021.solve_day03()
  end

  test "solve_day04/0" do
    assert {31424, 23042} == AdventOfCode2021.solve_day04()
  end

  test "solve_day05/0" do
    assert {6007, 19349} == AdventOfCode2021.solve_day05()
  end

  test "solve_day06/0" do
    assert {350_605, 1_592_778_185_024} == AdventOfCode2021.solve_day06()
  end

  test "solve_day07/0" do
    assert {336_131, 92_676_646} == AdventOfCode2021.solve_day07()
  end

  test "solve_day08/0" do
    assert {412, 978_171} == AdventOfCode2021.solve_day08()
  end

  test "solve_day09/0" do
    assert {633, 1_050_192} == AdventOfCode2021.solve_day09()
  end

  test "solve_day10/0" do
    assert {369_105, 3_999_363_569} == AdventOfCode2021.solve_day10()
  end

  test "solve_day11/0" do
    assert {1725, 308} == AdventOfCode2021.solve_day11()
  end

  test "solve_day14/0" do
    assert {3906, 4_441_317_262_452} == AdventOfCode2021.solve_day14()
  end
end
